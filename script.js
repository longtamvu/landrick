var nav = document.getElementById("navigator");
var header = document.getElementsByTagName("header");
var light ;
var buttonFill = document.getElementsByClassName("button-fill");
var textFill = document.getElementsByClassName("text-fill");
var full = document.getElementById("full-content");
var setting = document.getElementById("setting");
var colapse = document.getElementById("colapse");
var mainImg = document.getElementsByClassName("main-image");
var dark = document.getElementById("dark");
var light = document.getElementById("light");
var buttonWhite = document.getElementsByClassName("button-white");
var body = document.getElementsByTagName("body");
function blue() {
    for(var i = 0; i< buttonFill.length;i++)
    {
        buttonFill[i].classList.remove("button-blue","button-lightGreen", "button-purple","button-pink", "button-lightBlue","button-green","button-lightBlue2","button-lightPurple");
        buttonFill[i].classList.add("button-blue");
    }
    for(var i = 0; i< textFill.length;i++)
    {
        textFill[i].classList.remove("text-bluetext","text-lightGreen", "text-purple","text-pink", "text-lightBlue","text-green","text-lightBlue2","text-lightPurple");
        textFill[i].classList.add("text-blue");
    }
}
function lightGreen() {
    for(var i = 0; i< buttonFill.length;i++)
    {
        buttonFill[i].classList.remove("button-blue","button-lightGreen", "button-purple","button-pink", "button-lightBlue","button-green","button-lightBlue2","button-lightPurple");
        buttonFill[i].classList.add("button-lightGreen");
    }
    for(var i = 0; i< textFill.length;i++)
    {
        textFill[i].classList.remove("text-bluetext","text-lightGreen", "text-purple","text-pink", "text-lightBlue","text-green","text-lightBlue2","text-lightPurple");
        textFill[i].classList.add("text-lightGreen");
    }
}
function purple() {
    for(var i = 0; i< buttonFill.length;i++)
    {
        buttonFill[i].classList.remove("button-blue","button-lightGreen", "button-purple","button-pink", "button-lightBlue","button-green","button-lightBlue2","button-lightPurple");
        buttonFill[i].classList.add("button-purple");
    }
    for(var i = 0; i< textFill.length;i++)
    {
        textFill[i].classList.remove("text-bluetext","text-lightGreen", "text-purple","text-pink", "text-lightBlue","text-green","text-lightBlue2","text-lightPurple");
        textFill[i].classList.add("text-purple");
    }
}
function pink() {
    for(var i = 0; i< buttonFill.length;i++)
    {
        buttonFill[i].classList.remove("button-blue","button-lightGreen", "button-purple","button-pink", "button-lightBlue","button-green","button-lightBlue2","button-lightPurple");
        buttonFill[i].classList.add("button-pink");
    }
    for(var i = 0; i< textFill.length;i++)
    {
        textFill[i].classList.remove("text-bluetext","text-lightGreen", "text-purple","text-pink", "text-lightBlue","text-green","text-lightBlue2","text-lightPurple");
        textFill[i].classList.add("text-pink");
    }
}
function lightBlue() {
    for(var i = 0; i< buttonFill.length;i++)
    {
        buttonFill[i].classList.remove("button-blue","button-lightGreen", "button-purple","button-pink", "button-lightBlue","button-green","button-lightBlue2","button-lightPurple");
        buttonFill[i].classList.add("button-lightBlue");
    }
    for(var i = 0; i< textFill.length;i++)
    {
        textFill[i].classList.remove("text-bluetext","text-lightGreen", "text-purple","text-pink", "text-lightBlue","text-green","text-lightBlue2","text-lightPurple");
        textFill[i].classList.add("text-lightBlue");
    }
}
function green() {
    for(var i = 0; i< buttonFill.length;i++)
    {
        buttonFill[i].classList.remove("button-blue","button-lightGreen", "button-purple","button-pink", "button-lightBlue","button-green","button-lightBlue2","button-lightPurple");
        buttonFill[i].classList.add("button-green");
    }
    for(var i = 0; i< textFill.length;i++)
    {
        textFill[i].classList.remove("text-bluetext","text-lightGreen", "text-purple","text-pink", "text-lightBlue","text-green","text-lightBlue2","text-lightPurple");
        textFill[i].classList.add("text-green");
    }
}
function lightBlue2() {
    for(var i = 0; i< buttonFill.length;i++)
    {
        buttonFill[i].classList.remove("button-blue","button-lightGreen", "button-purple","button-pink", "button-lightBlue","button-green","button-lightBlue2","button-lightPurple");
        buttonFill[i].classList.add("button-lightBlue2");
    }
    for(var i = 0; i< textFill.length;i++)
    {
        textFill[i].classList.remove("text-bluetext","text-lightGreen", "text-purple","text-pink", "text-lightBlue","text-green","text-lightBlue2","text-lightPurple");
        textFill[i].classList.add("text-lightBlue2");
    }
}
function lightPurple() {
    for(var i = 0; i< buttonFill.length;i++)
    {
        buttonFill[i].classList.remove("button-blue","button-lightGreen", "button-purple","button-pink", "button-lightBlue","button-green","button-lightBlue2","button-lightPurple");
        buttonFill[i].classList.add("button-lightPurple");
    }
    for(var i = 0; i< textFill.length;i++)
    {
        textFill[i].classList.remove("text-bluetext","text-lightGreen", "text-purple","text-pink", "text-lightBlue","text-green","text-lightBlue2","text-lightPurple");
        textFill[i].classList.add("text-lightPurple");
    }
}

function dropdown(){
    nav.style.top = "60px";
    // nav.style.transform = "translate(0,100px)";
}
function dropdownReverse(){
    nav.style.top = "-100%";
    // nav.style.transform = "translate(0,-100px)";
}

var setting = document.getElementById("setting");
var colapse = document.getElementById("colapse");
function colapseHidden(){
    if(colapse.style.left === "-100%")
    {
        colapse.style.left = "0px";
        setting.style.left = "190px";
    }
    else{
        colapse.style.left = "-100%";
        setting.style.left = "0px";
    }
}
toTopButton= document.getElementById("toTop");

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) 
  {
    toTopButton.style.display = "block";
    if(light.style.display === "block"){
        header[0].style.backgroundColor = "#343a40";
        
    }
    else{
        header[0].style.backgroundColor = "#ffffff";
    }
  } else {
    toTopButton.style.display = "none";
    header[0].style.backgroundColor = "";
  }
}
function toTopFunc(){
    document.body.scrollTop = 0; 
    document.documentElement.scrollTop = 0; 
}
var landingMenu = document.getElementById("landing-menu");
function landingDropDownOnMouse() {
    var ilanding = document.getElementById("i-landing");
    landingMenu.style.visibility = "visible";
    ilanding.style.transform = "rotate(180deg)";
}

function landingDropDownOnMouseOut() {
    landingMenu.style.visibility = "hidden";
    var ilanding = document.getElementById("i-landing");
    ilanding.style.transform = "rotate(0deg)";
}
var pagesMenu = document.getElementById("pages-menu");
function pagesDropDownOnMouse() {
    var ipages = document.getElementById("i-pages");
    pagesMenu.style.visibility = "visible";
    ipages.style.transform = "rotate(180deg)";
}

function pagesDropDownOnMouseOut() {
    pagesMenu.style.visibility = "hidden";
    var ipages = document.getElementById("i-pages");
    ipages.style.transform = "rotate(0deg)";
}
var docsMenu = document.getElementById("docs-menu");
function docsDropDownOnMouse() {
    var idocs = document.getElementById("i-docs");
    docsMenu.style.visibility = "visible";
    idocs.style.transform = "rotate(180deg)";
}

function docsDropDownOnMouseOut() {
    docsMenu.style.visibility = "hidden";
    var idocs = document.getElementById("i-docs");
    idocs.style.transform = "rotate(0deg)";
}

function darkToLight(){
        dark.style.backgroundColor = "#ffffff";
        dark.style.color = "#343a40";
        full.style.backgroundColor = "#343a40";
        setting.style.backgroundColor = "#343a40";
        colapse.style.backgroundColor = "#343a40";
        full.style.color = "#ffffff";
        mainImg[0].style.backgroundColor = "#6c757d";
        light.style.display = "block";
        dark.style.display = "none";
        landingMenu.style.backgroundColor = "#343a40";
        pagesMenu.style.backgroundColor = "#343a40";
        docsMenu.style.backgroundColor = "#343a40";
        for(var i=0;i<buttonWhite.length;i++){
            buttonWhite.style.backgroundColor = "#6c757d !important";
        }
}
function lightToDark(){
    full.style.backgroundColor = "#ffffff";
        setting.style.backgroundColor = "#ffffff";
        colapse.style.backgroundColor = "#ffffff";
        full.style.color = "black";
        mainImg[0].style.backgroundColor = "#ecf6f8";
        light.style.display = "none";
        dark.style.display = "block";
        landingMenu.style.backgroundColor = "#ffffff";
        pagesMenu.style.backgroundColor = "#ffffff";
        docsMenu.style.backgroundColor = "#ffffff";
        for(var i=0;i<buttonWhite.length;i++){
            buttonWhite.style.backgroundColor = "#ffffff !important";
        }
}
var rtlv = document.getElementById("rtl");
var ltrv = document.getElementById("ltr"); 
var btnBuy = document.getElementById("btn-buyNow");
var logoFirst = document.getElementById("logo-first");
var pFooter = document.getElementById("p-footer");
var navbarMenu = document.getElementById("navbar-menu");
var navigation = document.getElementById("navigation");
function rtl(){
    full.style.direction ="rtl";
    ltrv.style.display = "block";
    rtlv.style.display = "none";
    mainImg[0].style.backgroundImage = "url('img/bg01C.png')";
    btnBuy.style.float = "left";
    logoFirst.style.float = "right";
    pFooter.style.textAlign = "right";
    landingMenu.style.left = "50%";
    pagesMenu.style.left = "42%";
    docsMenu.style.left="35%";
}
function ltr(){
    full.style.direction ="ltr";
    ltrv.style.display = "none";
    rtlv.style.display = "block";
    btnBuy.style.float = "right";
    logoFirst.style.float = "left";
    pFooter.style.textAlign = "left";
    mainImg[0].style.backgroundImage = "url('img/bg01.png')";
    landingMenu.style.left = "45%";
    pagesMenu.style.left = "50%";
    docsMenu.style.left="51%";
}
document.addEventListener('DOMContentLoaded',function(event){
    // array with texts to type in typewriter
    var dataText = [ "UI/UX Designer", "Web Developer", "Long"];
    
    // type one text in the typwriter
    // keeps calling itself until the text is finished
    function typeWriter(text, i, fnCallback) {
      // chekc if text isn't finished yet
      if (i < (text.length)) {
        // add next character to h1
       document.querySelector("h1").innerHTML = text.substring(0, i+1) +'<span aria-hidden="true"></span>';
  
        // wait for a while and call this function again for next character
        setTimeout(function() {
          typeWriter(text, i + 1, fnCallback)
        }, 200);
      }
      // text finished, call callback if there is a callback function
      else if (typeof fnCallback == 'function') {
        // call callback after timeout
        setTimeout(fnCallback, 700);
      }
    }
    // start a typewriter animation for a text in the dataText array
     function StartTextAnimation(i) {
       if (typeof dataText[i] == 'undefined'){
          setTimeout(function() {
            StartTextAnimation(0);
          }, 1000);
       }
       // check if dataText[i] exists
      if (i < dataText[i].length) {
        // text exists! start typewriter animation
       typeWriter(dataText[i], 0, function(){
         // after callback (and whole text has been animated), start next text
         StartTextAnimation(i + 1);
       });
      }
    }
    // start the text animation
    StartTextAnimation(0);
  });